
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const cluster = require('cluster');

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// get application config store
const appConfigs = require('./src/initializers/AppConfigStore');

if(appConfigs.cluster) {
    if(cluster.isMaster) {

        var numWorkers = process.env.ENV_WORKER_COUNT || 1;
    
        for(var i = 0; i < numWorkers; i++) {
            cluster.fork();
        }
    
        cluster.on('online', () => {
          // do nothing
        });
    
        cluster.on('exit', () => {
            cluster.fork();
        });
    
        cluster.on('disconnect', () => {
            cluster.fork();
        });
    } else {
        startServer();
    }
} else {
    startServer();
}

function startServer() {

    const domain = require('domain');
    const server = express();
    module.exports = server; // for testing

    server.use((req, res, next) => {

        var d = domain.create();  
        d.on('error', () => {
            try {

                //make sure we close down within 30 seconds
                var killtimer = setTimeout(function() {
                    process.exit(1);
                }, 30000);

                // But don't keep the process open just for that!
                killtimer.unref();

                //stop taking new requests.
                server.close();

                //Let the master know we're dead.  This will trigger a
                //'disconnect' in the cluster master, and then it will fork
                //a new worker.
                cluster.worker.disconnect();

                //send an error to the request that triggered the problem
                res.statusCode = 500;
                res.setHeader('content-type', 'text/plain');
                res.end('Oops, there was a problem!\n');
            } catch (err2) {
                // oh well, not much we can do at this point.
            }
        });

        // Because req and res were created before this domain existed,
        // we need to explicitly add them.
        d.add(req);
        d.add(res);

        //Now run the handler function in the domain.
        d.run(next);
    });

    // middlewares: body parser, cors
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: true }));
    server.use(cors());

    // ==================================================================================================
    // SWAGGER CONFIGURATION: START

    const swaggerDefinition = {
        info: {
            title: 'REST API for SBE-Master-Data',
            version: '0.1.1', // Version of the app
            description: 'Restful API documentation for SBE-Master-Data',
        },
        host: 'localhost:3000',
        basePath: '/api/master-data/v1/'
    };

    // options for the swagger docs
    const swaggerOptions = {
        // import swaggerDefinitions
        swaggerDefinition,
        // path to the API docs
        apis: ['./src/api-docs/definitions.yaml', './src/api-docs/**/*.yaml'],
    };

    console.log('swaggerOptions', swaggerOptions);

    // initialize swagger-jsdoc
    const swaggerSpec = swaggerJSDoc(swaggerOptions);

    // use swagger-Ui-express for your app documentation endpoint
    server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

    var longPoll = require('./src/modules/location/LongPolling');
    longPoll.longPoll();

    // SWAGGER CONFIGURATION: END

    // ==================================================================================================
    // INTIALIZER CONFIGURATION: START

    // initializer
    // load controllers
    require('./src/modules/projects/ProjectController')();

    // bootstrap database
    require('./src/initializers/Sequelizer');
    // load postgres database structure
    require('./src/initializers/ORMMapper');
    // INTIALIZER CONFIGURATION: END

    // ==================================================================================================
    // START SERVER: START
    
    server.listen(3000, function() {
        console.log('Server is running at ', 3000);
    });
};






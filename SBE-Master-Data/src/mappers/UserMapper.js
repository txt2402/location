
var Sequelize = require('sequelize');
var sequelizer = require('../initializers/Sequelizer');
const DatabaseConstants = require('../constants/DatabaseConstants');

var UserMapper = sequelizer.define('users', {
    
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.STRING(100),
        unique: true
    },
    user_secret: {
        type: Sequelize.STRING(256)
    }
}, {
    underscored: true,
    freezeTableName: true,
    schema: DatabaseConstants.FIRSTFIRE_SCHEMA
});

module.exports = UserMapper;
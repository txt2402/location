
var Sequelize = require('sequelize');
var sequelizer = require('../initializers/Sequelizer');
const DatabaseConstants = require('../constants/DatabaseConstants');

var TestOrderMapper = sequelizer.define('test-orders', {
    
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    code: {
        type: Sequelize.STRING(100),
        unique: true
    },
    suites: {
        type: Sequelize.STRING(500)
    }
}, {
    underscored: true,
    freezeTableName: true,
    schema: DatabaseConstants.FIRSTFIRE_SCHEMA
});

module.exports = TestOrderMapper;
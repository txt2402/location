
var Sequelize = require('sequelize');
var sequelizer = require('../initializers/Sequelizer');
const DatabaseConstants = require('../constants/DatabaseConstants');

var TestExecutionMapper = sequelizer.define('test-executions', {
    
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    code: {
        type: Sequelize.STRING(100),
        unique: true
    }
}, {
    underscored: true,
    freezeTableName: true,
    schema: DatabaseConstants.FIRSTFIRE_SCHEMA
});

module.exports = TestExecutionMapper;
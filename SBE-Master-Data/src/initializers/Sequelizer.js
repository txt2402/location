
const BaseSequelizer = require('sbe-core').BaseSequelizer;
const appConfigs = require('./AppConfigStore');

var baseSequelizer = new BaseSequelizer(appConfigs.sequelize);
var sequelizer = baseSequelizer._sequelizer;

module.exports = sequelizer;


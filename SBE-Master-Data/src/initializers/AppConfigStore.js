
const path = require('path');
const BaseAppConfig = require('sbe-core').BaseAppConfig;

var configPath = path.join(__dirname, '../../resources/application.yaml')
var baseAppConfig = new BaseAppConfig(configPath);
var configs = baseAppConfig._configs.app;

module.exports = configs;


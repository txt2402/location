
var sequelizer = require('./Sequelizer');

var UserMapper = require('../mappers/UserMapper');
var ProjectMapper = require('../mappers/ProjectMapper');
var TestOrderMapper = require('../mappers/TestOrderMapper');

// ProjectMapper n - n UserMapper
UserMapper.belongsToMany(ProjectMapper, {
  as: 'projects',
  through: 'project_users',
  foreignKey: 'user_id'
});

ProjectMapper.belongsToMany(UserMapper, {
  as: 'projects',
  through: 'project_users',
  foreignKey: 'project_id'
});

// ProjectMapper 1 - n TestExecutionMapper
ProjectMapper.hasMany(TestOrderMapper, {
  as: 'testOrders'
});

// TestOrderMapper 1 - n TestExecutionMapper
// TestOrderMapper.hasMany(TestExecutionMapper, {
//   as: 'executions'
// });

// ==============================================================================
// then sync
sequelizer.sync({force: false}).then(() => {
  //do nothing
  console.log('database has synced');
});  


const DataFeed = require('./DataFeed');
const dgram = require('dgram');
const server = dgram.createSocket('udp4');

const clients = DataFeed.clientPool();

//Server Configuration
server.on('error', (error)=>{
    console.log(error);
})
server.on('message',(msg,rinfo)=>{
    console.log(`Got ${msg} from ${rinfo.address}`)
})
server.on('listening',()=>{
    var address = server.address();
    console.log(`Server is listening on ${address.address}:${address.port}`)
})
server.bind(41234);


function LongPollingManager(){

    this.longPoll = _longPoll;
}

var _longPoll = function(){
    setInterval(function(){
        if (clients[0]){ //if there is at least 1 client
            for (var c in clients){
                if (clients[c]["Status"] ==1){ //if client is still connecting
                    var message = Buffer.from("Long: "+ clients[c]["Longitude"]+" Lat: "+clients[c]["Latitude"]+ " IP Address: "+ clients[c]["IP Address"])
                    server.send(message,5000,'localhost') //Send location to receiving server on port 5000
                    clients[c]["Status"] = 0 //Location of client has been sent;

                }
            }
        } 
  
      
    },5000)
}
module.exports= new LongPollingManager
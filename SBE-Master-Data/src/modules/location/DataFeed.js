var clients= [];
function DataFeed(){
    this.clientManager = _clientManager;
    this.clientPool = _clientPool;
}

var _clientManager = function(long,lat,ip){
    var client = {
        "Longitude": long,
        "Latitude": lat,
        "IP Address": ip,
        "Status": 1 //1 is connecting 0 is disconnected
    }
    if (!clients[0]){
        clients.push(client);
    }
    else{
        var ifExisted = false; // check if IP address has been stored or not
        for (var c in clients){
            if (client["IP Address"] == clients[c]["IP Address"]){
                ifExisted = true;
                clients[c]["Status"] = 1 // client still connecting;
                break;
            }
        }
        if (ifExisted == false) clients.push(client); // if client hasn't been stored, store client
    }
    console.log(client);
}
var _clientPool = function(){
    return clients;
}
module.exports = new DataFeed
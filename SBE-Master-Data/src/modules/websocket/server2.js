const WebSocket = require('ws');

var ws = new WebSocket('wss://ServerA:3000/', 'echo-protocol');
 ws.onopen = function () {
     console.log('socket connection opened properly');
     ws.send("Hello World"); // send a message
     console.log('message sent');
 };

 ws.onmessage = function (evt) {
     console.log("Message received = " + evt.data);
 };

 ws.onclose = function () {
     // websocket is closed.
     console.log("Connection closed...");
 };
 ws.onerror = (err) =>{
    console.log(err);
 }
const levelup = require('levelup');
const leveldown = require('leveldown');
const express = require('express');
const app = express();
const math = require('mathjs');
const data = require ('./data.json');
const r = 6371e3;
var long2;
var lat2;
var delta_lat;
var delta_long;
var a,c,d;
var db = new levelup(leveldown('./db')); 
function toRadians(degrees){
  return degrees * math.PI/180;
}
function Distance(long,lat){
  long = toRadians(long);
  lat = toRadians(lat);
  for(var i =0;i<data.length;i++){
    long2 = toRadians(data[i].long);
    lat2 = toRadians(data[i].lat);
    delta_lat = toRadians(lat2 - lat);
    delta_long = toRadians(long2 - long);
    a = math.pow(math.sin(delta_lat/2),2) + math.pow(math.sin(delta_long/2),2) * math.cos(lat) * math.cos(lat2);
    c = 2* math.asin(math.sqrt(a));
    d = r*c/1000;
    if (d<=5) {
      db.put('position'+i,data[i].name, (err)=>{
          if (err) return console.log(err);
      });
    } 
  }
}
app.get('/:long/:lat/',(req,res) =>{
  var long = req.params.long;
  var lat = req.params.lat;
  Distance(long,lat);
  console.log('Done.');
  res.send('Check console');
  db.createValueStream().on('data', function (data) {
    console.log(data.toString('utf8'));
  });
});
app.listen(3000);
var clients = [];

function DataFeed(){
    this.clientManager = _clientManager;
    this.clientsPool = _clientPool;
}

var _clientManager = (rinfo)=>{
    if (!clients[0]){
        var client = {
            "Port": rinfo.port,
            "Status": 1,
        }
        clients.push(client);
    }
    else{
        var temp = false;
        for (var c in clients){
            if (clients[c]["Port"]==rinfo.port) {
                clients[c]["Status"]=1;
                temp = true;
            }
        }
        if (temp == false){
            var client = {
                "Port": rinfo.port,
                "Status": 1,
            }
            clients.push(client);
        }
    }

}

var _clientPool = ()=>{
    return clients;
}


module.exports = new DataFeed;

const dgram = require('dgram');
const server = dgram.createSocket('udp4');
const DataFeed = require('./DataFeed');


// module.exports = function(){

    server.on('error', (err) =>{
        console.log(err);
    })
    server.on('message',(msg,rinfo)=>{

        DataFeed.clientManager(rinfo);
    })
    server.on('listening',()=>{
        var address = server.address();
        console.log(`Server is listening in ${address.address}:${address.port}`);
    })
    server.bind(41234);

    var longPoll = function() {

            setInterval(()=>{
                var clients = DataFeed.clientsPool();
                for (var c in clients){
                    
                    if ( clients[c]["Status"] == 1){
                        server.send("Respones", clients[c]["Port"],'localhost');
                        clients[c]["Status"]=0;
                    }
                    else {
                        console.log("Client on port " + clients[c]["Port"]+ " is disconnected" );
                    }
                }

                console.log("Polling...");
            },5000)

        }
    longPoll();

// }

module.exports = server;

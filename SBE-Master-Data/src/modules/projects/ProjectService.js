
function ProjectService() {
    this.hello = _hello;
    this.createProjects = _createProjects;
}

var _hello = async function(req) {
    return {
        code: 'hello world',
        message: 'hello world'
    };
}

var _createProjects = async function(req) {

    // return a service status
    return {

    };
}

module.exports = new ProjectService();
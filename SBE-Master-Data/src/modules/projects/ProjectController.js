
var RestController = require('sbe-core').RestController;
var server = require('../../../server');
var projectService = require('./ProjectService');
var DataFeed = require('../location/DataFeed');
var fs = require('fs');
// we can create our own filters and pass it as argument of RestController.processRequest
var filters = [];
var otherFilters = [];

// validators
var validators = [];
var otherValidators = [];

module.exports = function() {

    // hello world
    server.get('/master-data/v1/project', function(req, res) {
        RestController.processRequest(req, res, null, null, projectService.hello);
    });

    server.get('/',(req,res)=>{ // GET FRONTEND
        fs.readFile('src/modules/location/location.html',(error,data)=>{
            res.writeHead(200,'Content type: text/html')
            res.write(data);
            res.end();
        })
    })
    // create project(s)
    server.post('/',(req,res)=>{
        var ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || //GET IP ADDRESS
        req.connection.remoteAddress || 
        req.socket.remoteAddress || 
        req.connection.socket.remoteAddress

        DataFeed.clientManager(req.body.Long,req.body.Lat,ip);
    })
    
    server.post('/master-data/v1/project', function(req, res) {
        RestController.processRequest(req, res, filters, validators, projectService.createProjects);
    });

    // udpate project(s)
    server.put('/master-data/v1/project', function(req, res) {
        RestController.processRequest(req, res, otherFilters, otherValidators, projectService.updateProjects);
    });

    // .... other apis
}
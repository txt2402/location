
module.exports = function (grunt) {
	grunt.initConfig({
		copy: {
			dev: {
				files: [
					{
						expand: true,
						cwd: 'resources/dev/',
						src: ['*.yaml', '*.yml'],
						dest: 'resources/'
					}
				]
			},
			integration: {
				files: [
					{
						expand: true,
						cwd: 'resources/integration/',
						src: ['*.yaml', '*.yml'],
						dest: 'resources/'
					}
				]
			},
			testing: {
				files: [
					{
						expand: true,
						cwd: 'resources/testing/',
						src: ['*.yaml', '*.yml'],
						dest: 'resources/'
					}
				]
			},
			staging: {
				files: [
					{
						expand: true,
						cwd: 'resources/staging/',
						src: ['*.yaml', '*.yml'],
						dest: 'resources/'
					}
				]
			},
			prod: {
				files: [
					{
						expand: true,
						cwd: 'resources/prod/',
						src: ['*.yaml', '*.yml'],
						dest: 'resources/'
					}
				]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');

	if (process.env.NODE_ENV === 'dev') {
		grunt.registerTask('copy-files', ['copy:dev']);
	}
	else if (process.env.NODE_ENV === 'integration') {
		grunt.registerTask('copy-files', ['copy:integration']);
	}
	else if (process.env.NODE_ENV === 'testing') {
		grunt.registerTask('copy-files', ['copy:testing']);
	}
	else if (process.env.NODE_ENV === 'staging') {
		grunt.registerTask('copy-files', ['copy:staging']);
	}
	else if (process.env.NODE_ENV === 'prod') {
		grunt.registerTask('copy-files', ['copy:prod']);
	}
}
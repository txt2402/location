
function HttpOperationConstants() {}

HttpOperationConstants.GET_INIT_DATA = 'getInitData';
HttpOperationConstants.FIND_ONE = 'findOne';
HttpOperationConstants.FIND_ALL = 'findAll';
HttpOperationConstants.SAVE = 'save';
HttpOperationConstants.UPDATE = 'update';
HttpOperationConstants.DELETE = 'remove';

module.exports = HttpOperationConstants;
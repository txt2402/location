
function StatusConstants() {}

StatusConstants.ACKNOWLEDGED = 'acknowledged';
StatusConstants.FOUND = 'found';
StatusConstants.OK = 'ok';
StatusConstants.VALIDATOR_FAILED = 'validator failed';
StatusConstants.INTERNAL_ERROR = 'internal error';
StatusConstants.UNAUTHORISED = 'unauthorised';
StatusConstants.BAD_REQUEST = 'bad request';
StatusConstants.FORBIDDEN = 'forbidden';

module.exports = StatusConstants;
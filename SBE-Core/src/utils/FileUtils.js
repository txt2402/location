
function FileUtils() {}

const fs = require('fs');

FileUtils.changeFileNameByTimestamp = (file) => {
  var timestamp = new Date().getTime();
  var fileOriginalName = file.originalname;
  var fileSize = file.size;
  file.originalname = timestamp + fileOriginalName.substring(fileOriginalName.lastIndexOf("."));
  return file;
}

FileUtils.store = (folder, file) => {
  fs.writeFileSync(folder + file.originalname, file.buffer);
}

module.exports = FileUtils;
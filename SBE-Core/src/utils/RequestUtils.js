
function RequestUtils() {}

RequestUtils.getSingleFile = function(req) {

  var file = null;
  const files = req.files;

  if(files) {
    file = req.files.file;
  }

  if(file && (file.length > 0)) {
    file = file[0];
  }
  
  return file;
}

module.exports = RequestUtils;
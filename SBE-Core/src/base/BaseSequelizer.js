
var Sequelize = require('sequelize');

function BaseSequelizer(databaseConfigs) {
    this._databaseConfigs = databaseConfigs;
    this._sequelizer = null;
    this.init();
}

BaseSequelizer.prototype.init = function() {
    if(this._databaseConfigs) {
        var databaseConfigs = this._databaseConfigs;
        this._sequelizer = new Sequelize(databaseConfigs.name, databaseConfigs.user, databaseConfigs.pwd, {
            host: databaseConfigs.host,
            dialect: databaseConfigs.provider,
            pool: {
                max: databaseConfigs.poolSizeMax,
                min: databaseConfigs.poolSizeMin,
                idle: databaseConfigs.idle,
                acquire: databaseConfigs.acquire
            },
            logging: databaseConfigs.logging
        });
    }

    this._sequelizer.authenticate()
    .then(() => {
        console.log('Connection to Postgres database has been successful');
    })
    .catch(err => {
        throw Error('Unable to connect to Postgres database, with errorStack', err.stack);
    });
}

module.exports = BaseSequelizer;
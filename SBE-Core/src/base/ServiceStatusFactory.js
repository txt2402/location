
const StatusConstants = require('../constants/StatusConstants');
var ServiceStatus = require('./ServiceStatus');

function ServiceStatusFactory() {}

ServiceStatusFactory.prototype.getStatus = function(code, message, commandId, data) {
  if(code === StatusConstants.INTERNAL_ERROR ||
    code === StatusConstants.BAD_REQUEST ||
    code === StatusConstants.UNAUTHORISED || 
    code === StatusConstants.FORBIDDEN) {

    return new ServiceStatus(500, code, message);
  }

  if(code === StatusConstants.VALIDATOR_FAILED) {
    return new ServiceStatus(402, code, message);
  }

  if(code === StatusConstants.ACKNOWLEDGED) {
    if(commandId) {
      return new ServiceStatus(200, code, message, commandId);
    }
    return new ServiceStatus(200, code, message);
  }

  if(code === StatusConstants.FOUND) {
    return new ServiceStatus(200, code, message, data);
  }

  if(code === StatusConstants.OK) {
    if(data) {
      return new ServiceStatus(200, code, message, null, data);
    }

    return new ServiceStatus(200, code, message);
  }
  
  return null;
}

module.exports = ServiceStatusFactory;
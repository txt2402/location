
var yaml = require('js-yaml');
var fs   = require('fs');

function BaseAppConfig(configPath) {
    this._configPath = configPath;
    this.init();
}

BaseAppConfig.prototype.init = function() {
  
  this._configs = {};

  // get config items from database.yaml
  let appConfigs;
  try {
    appConfigs = yaml.safeLoad(fs.readFileSync(this._configPath, 'utf8'));
  } catch (e) {
    throw Error('app Config Store file is not reachable');
  }

  if(appConfigs) {
    this._configs.app = appConfigs;
  }
}

module.exports = BaseAppConfig;
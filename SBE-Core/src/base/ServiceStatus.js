
function ServiceStatus(status, code, message, commandId, data) {
    this.status = status;
    this.code = code;
    this.message = message;
    if(commandId) {
      this.commandId = commandId;
    }
    if(data) {
      this.data = data;
    }
  }
  
  module.exports = ServiceStatus;
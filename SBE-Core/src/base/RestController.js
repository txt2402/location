
const _ = require('lodash');
const ServiceStatusFactory = require('./ServiceStatusFactory');
const StatusConstants = require('../constants/StatusConstants');

function RestController() {}

var shouldFilterValid = function(req, filters) {

    if(!filters) {
        return null;
    }

    if(filters && !_.isArray(filters)) {
        return ServiceStatusFactory.getStatus(StatusConstants.VALIDATOR_FAILED, 'filters are not valid');
    }

    if(filters.length == 0) {
        return null;
    }

    var filterStatus = null;
    filterStatus = filters.forEach(filter => {
        if(!filter['name']) {
            return ServiceStatusFactory.getStatus(StatusConstants.VALIDATOR_FAILED, 'some filter didn\'t have its name');
        }

        if(!filter['shouldValid']) {
            return ServiceStatusFactory.getStatus(StatusConstants.VALIDATOR_FAILED, 'filter ' + filter['name'] + ' don\'t have shouldValid method');
        }

        filterStatus = filter._shouldValid(req);
        if(filterStatus) {
            return filterStatus;
        }
    });

    return filterStatus;
}

var shouldValidatorValid = function(req, validators) {

    if(!validators) {
        return null;
    }

    if(validators && !_.isArray(validators)) {
        return ServiceStatusFactory.getStatus(StatusConstants.VALIDATOR_FAILED, 'validators are not valid');
    }

    if(filters.length == 0) {
        return null;
    }

    var validatorStatus = null;
    validatorStatus = validators.forEach(validator => {
        if(!validator['name']) {
            return ServiceStatusFactory.getStatus(StatusConstants.VALIDATOR_FAILED, 'some validator didn\'t have its name');
        }

        if(!validator['shouldValid']) {
            return ServiceStatusFactory.getStatus(StatusConstants.VALIDATOR_FAILED, 'validator ' + validator['name'] + ' don\'t have shouldValid method');
        }

        validatorStatus = validator._shouldValid(req);
        if(validatorStatus) {
            return validatorStatus;
        }
    });

    return validatorStatus;
}

var sendError = function(res, serviceStatus) {
    res.json(serviceStatus);
}

RestController.processRequest = function(req, res, filters, validators, service) {

    // check filter status
    var filterStatus = shouldFilterValid(req, filters)
    if(filterStatus) {
        sendError(res, filterStatus);
    }

    // check validator status
    var validatorStatus = shouldValidatorValid(req, validators);
    if(validatorStatus) {
        sendError(res, validatorStatus);
    }

    service(req)
    .then(serviceStatus => {
        console.log('process service ', service.name);
        res.json(serviceStatus);
    })
    .catch(err => {
      console.log('process service ', service.name, ' got error ', err.stack);
    });
}

module.exports = RestController;